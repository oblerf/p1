// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/OpenPlayerController.h"

#include "Game/OpenGameState.h"
#include "Interaction/InteractInterface.h"

void AOpenPlayerController::UpdateOverlapEvent_Implementation(AActor* Actor, const FGameplayTag& EventTag)
{
	if (Actor->Implements<UInteractInterface>())
	{
		IInteractInterface::Execute_UpdateOverlapEvent(Actor, EventTag);
	}
}

void AOpenPlayerController::PostPawnOnBeginOverlapActor_Implementation(AActor* Actor)
{
	if (Actor->Implements<UInteractInterface>())
	{
		IInteractInterface::Execute_OnBeginOverlapPlayerPawn(Actor);
	}
}

void AOpenPlayerController::UpdateGlobalEvent_Implementation(const FGameplayTag& EventTag)
{
	if (const auto OpenGameState = Cast<AOpenGameState>(GetWorld()->GetGameState()))
	{
		OpenGameState->Server_UpdateGlobalWeatherEventTag(EventTag);
	}
}

void AOpenPlayerController::PostPawnOnEndOverlapActor(AActor* Actor)
{
	if (Actor->Implements<UInteractInterface>())
	{
		IInteractInterface::Execute_OnEndOverlapPlayerPawn(Actor);
	}
}
