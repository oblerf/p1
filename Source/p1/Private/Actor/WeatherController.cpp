// Fill out your copyright notice in the Description page of Project Settings.


#include "Actor/WeatherController.h"

#include "Components/BoxComponent.h"
#include "Game/OpenGameState.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Net/UnrealNetwork.h"
#include "Particles/ParticleSystemComponent.h"

// Sets default values
AWeatherController::AWeatherController()
{
	SetReplicates(true);

	CurrentWeather = FWeatherData();

	PrimaryActorTick.bCanEverTick = true;
	SetActorTickInterval(1.f);
	Box = CreateDefaultSubobject<UBoxComponent>("Box");
	Box->SetupAttachment(GetRootComponent());

	SunnySystem = CreateDefaultSubobject<UParticleSystemComponent>("SunnySystem");
	SunnySystem->SetupAttachment(Box);
	SunnySystem->SetAutoActivate(false);
	WeatherParticleSystems.Add(Sunny, SunnySystem);

	RainySystem = CreateDefaultSubobject<UParticleSystemComponent>("RainySystem");
	RainySystem->SetupAttachment(Box);
	RainySystem->SetAutoActivate(false);
	WeatherParticleSystems.Add(Rainy, RainySystem);

	SnowySystem = CreateDefaultSubobject<UParticleSystemComponent>("SnowySystem");
	SnowySystem->SetupAttachment(Box);
	SnowySystem->SetAutoActivate(false);
	WeatherParticleSystems.Add(Snowy, SnowySystem);

	WindySystem = CreateDefaultSubobject<UParticleSystemComponent>("WindySystem");
	WindySystem->SetupAttachment(Box);
	WindySystem->SetAutoActivate(false);
	WeatherParticleSystems.Add(Windy, WindySystem);

	BloodMoonSystem = CreateDefaultSubobject<UParticleSystemComponent>("BloodMoonSystem");
	BloodMoonSystem->SetupAttachment(Box);
	BloodMoonSystem->SetAutoActivate(false);
	WeatherParticleSystems.Add(BloodMoon, BloodMoonSystem);

	SolarEclipseSystem = CreateDefaultSubobject<UParticleSystemComponent>("SolarEclipseSystem");
	SolarEclipseSystem->SetupAttachment(Box);
	SolarEclipseSystem->SetAutoActivate(false);
	WeatherParticleSystems.Add(SolarEclipse, SolarEclipseSystem);
}

void AWeatherController::BeginPlay()
{
	Super::BeginPlay();
	if (GetNetMode() == NM_Client)
	{
		return;
	}
	if (const auto OpenGameState = Cast<AOpenGameState>(GetWorld()->GetGameState()))
	{
		OpenGameState->OnUpdateGlobalWeatherEvent.AddUObject(this, &AWeatherController::GlobalWeatherEventUpdate);
	}
	Execute_OnBeginOverlapPlayerPawn(this);
}

void AWeatherController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (CheckIfFirstPawnOverlap())
		UKismetSystemLibrary::PrintString(GetWorld(), FString::Printf(
			                                  TEXT(
				                                  "当前所在Tag:%s 当前天气=%s 持续至Days=%d Hours=%d"),
			                                  *WCTag.ToString(),
			                                  *UEnum::GetDisplayValueAsText(CurrentWeather.WeatherType).ToString(),
			                                  CurrentWeather.EndTime.Days, CurrentWeather.EndTime.Hours), true, true,
		                                  FLinearColor(0, .66, 1),
		                                  1);

	if (GetNetMode() == NM_Client)
	{
		return;
	}
	const auto OpenGameState = Cast<AOpenGameState>(GetWorld()->GetGameState());
	if (CurrentEvent.Duration != 0 && OpenGameState->CurrentTime >= CurrentEvent.EndTime)
	{
		Server_UpdateWeather(CurrentEvent.NextEventTag);
	}
	else if (OpenGameState->CurrentTime >= CurrentWeather.EndTime)
	{
		Server_UpdateWeather(CurrentEvent.EventTag);
	}
}

void AWeatherController::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AWeatherController, CurrentWeather);
}

void AWeatherController::UpdateOverlapEvent_Implementation(const FGameplayTag& EventTag)
{
	// UKismetSystemLibrary::PrintString(GetWorld(), FString::Printf(
	// 	                                  TEXT(
	// 		                                  "UpdateOverlapWeather_Implementation EventTag=%s"),
	// 	                                  *EventTag.ToString()));
	Server_UpdateWeather(EventTag);
}

void AWeatherController::OnBeginOverlapPlayerPawn_Implementation()
{
	const auto OpenGameState = Cast<AOpenGameState>(GetWorld()->GetGameState());
	if (OpenGameState->WeatherControllerTagWithHandle.Contains(WCTag))
	{
		CurrentEvent = OpenGameState->WeatherControllerTagWithHandle[WCTag].Event;
		CurrentWeather = OpenGameState->WeatherControllerTagWithHandle[WCTag].Weather;
		UpdateWeatherActive();
	}
	else
	{
		FGameplayTag EventTag = OpenGameState->GlobalWeatherEventTag;
		if (EventTag == FGameplayTag())
		{
			EventTag = OpenGameState->WeatherChangeData->WeatherDefaultEventTag;
		}
		Server_UpdateWeather(EventTag);
	}
}

void AWeatherController::OnEndOverlapPlayerPawn_Implementation()
{
	for (auto Element : WeatherParticleSystems)
	{
		Element.Value->SetActive(false);
	}
}

void AWeatherController::OnRep_CurrentWeather()
{
	UpdateWeatherActive();
}

void AWeatherController::UpdateWeatherActive()
{
	if (!CheckIfFirstPawnOverlap())return;
	for (auto Element : WeatherParticleSystems)
	{
		if (Element.Key == CurrentWeather.WeatherType)
			continue;
		Element.Value->SetActive(false);
	}
	if (WeatherParticleSystems.Contains(CurrentWeather.WeatherType))
	{
		WeatherParticleSystems[CurrentWeather.WeatherType]->SetActive(true);
	}
}

bool AWeatherController::CheckIfFirstPawnOverlap() const
{
	return Box->IsOverlappingActor(GetWorld()->GetFirstPlayerController()->GetPawn());
}

void AWeatherController::GlobalWeatherEventUpdate(const FGameplayTag& EventTag)
{
	Server_UpdateWeather(EventTag);
}


void AWeatherController::Multicast_ReceiveWeatherUpdate_Implementation(const FWeatherData& Weather)
{
	CurrentWeather = Weather;
	// UKismetSystemLibrary::PrintString(GetWorld(), FString::Printf(
	// 	                                  TEXT(
	// 		                                  "Multicast_ReceiveWeatherUpdate_Implementation CurrentWeather=%s Duration=%d"),
	// 	                                  *UEnum::GetDisplayValueAsText(CurrentWeather.WeatherType).ToString(),
	// 	                                  CurrentWeather.Duration));
	UpdateWeatherActive();
}

void AWeatherController::Server_UpdateWeather_Implementation(const FGameplayTag& EventTag)
{
	// UKismetSystemLibrary::PrintString(GetWorld(), FString::Printf(
	// 	                                  TEXT(
	// 		                                  "Server_UpdateWeather_Implementation EventTag=%s"),
	// 	                                  *EventTag.ToString()));
	const auto OpenGameState = Cast<AOpenGameState>(GetWorld()->GetGameState());
	FWeatherData OutWeatherData;
	FWeatherEvent OutEventData;
	if (OpenGameState &&
		OpenGameState->WeatherChangeData &&
		OpenGameState->WeatherChangeData.Get()->RandWeatherFromEvent(
			EventTag, CurrentWeather.WeatherType,
			OutWeatherData, OutEventData))
	{
		OutWeatherData.EndTime = OpenGameState->CurrentTime.Add(FTimeData(OutWeatherData.Duration, 0),
		                                                        OpenGameState->DayLength);
		OutEventData.EndTime = OpenGameState->CurrentTime.Add(FTimeData(OutEventData.Duration, 0),
		                                                      OpenGameState->DayLength);
		Multicast_ReceiveWeatherUpdate(OutWeatherData);
		CurrentEvent = OutEventData;
		auto NewHandle = FEventWithWeatherHandle();
		NewHandle.Event = CurrentEvent;
		NewHandle.Weather = CurrentWeather;
		OpenGameState->TryToAddControllerTagWithHandle(WCTag, NewHandle);
	}
}
