// Fill out your copyright notice in the Description page of Project Settings.


#include "Data/WeatherChange.h"


bool UWeatherChange::RandWeatherFromEvent(const FGameplayTag& Event, const TEnumAsByte<EWeatherType>& Weather,
                                          FWeatherData& OutWeatherData, FWeatherEvent& OutEventData)
{
	for (auto Element : WeatherEvents)
	{
		if (Element.EventTag != Event)continue;
		if (Element.WeatherDataInfos.Num() == 0)continue;
		if (Element.WeatherDataInfos.Num() == 1 && Element.WeatherDataInfos[0].WeatherType == Weather)continue;

		auto TotalProb = 0;
		for (auto Data : Element.WeatherDataInfos)
		{
			if (Data.WeatherType == Weather)continue;
			TotalProb += Data.Prob;
		}

		auto Rand = FMath::RandRange(0, TotalProb);
		for (auto Data : Element.WeatherDataInfos)
		{
			if (Data.WeatherType == Weather)continue;
			if (Rand <= Data.Prob)
			{
				OutWeatherData = Data;
				OutEventData = Element;
				return true;
			}
			Rand -= Data.Prob;
		}
	}

	return false;
}
