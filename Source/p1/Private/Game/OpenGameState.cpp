// Fill out your copyright notice in the Description page of Project Settings.


#include "Game/OpenGameState.h"

#include "Kismet/KismetSystemLibrary.h"
#include "Net/UnrealNetwork.h"

AOpenGameState::AOpenGameState()
{
	CurrentTime.Hours = 6;
	PrimaryActorTick.bCanEverTick = true;
	SetActorTickInterval(1.f);
}

void AOpenGameState::BeginPlay()
{
	Super::BeginPlay();
	check(WeatherChangeData)
	GlobalWeatherEventTag = WeatherChangeData->WeatherDefaultEventTag;

	UKismetSystemLibrary::PrintString(GetWorld(), FString::Printf(
		                                  TEXT("天气切换与相应粒子效果因素材原因仅使用同一种雪花粒子效果，不同天气更换不同雪花颜色以作区分\n"
			                                  "普通事件所含天气：晴天-黄色; 雨天-蓝色; 雪天-白色; 刮风天-绿色\n"
			                                  "特殊事件所含天气：血月-红色; 日食-黑色 持续24小时(现实24s)\n\n"
			                                  "粒子系统性能相关优化：对应天气切换系统仅在系统内包含了该客户端玩家时生效，若不包含游戏内的主控玩家，则该特效在该客户端不显示\n"
			                                  "例：有一主机(ListenServer)玩家A位于(0.0)对应的天气切换系统内,一客机玩家B位于(1,1)对应的天气切换系统内\n"
			                                  "则切换系统仅对其所在区域生成粒子,玩家B的客户端里的(0.0)切换系统将不会生成粒子\n\n"
			                                  "按P键将玩家所在区域切换为特殊事件并改变天气\n"
			                                  "按L键将所有(正在生效的)地区切换为特殊事件并改变天气")
	                                  ), true, true, FLinearColor(0, .66, 1),
	                                  50);
}

void AOpenGameState::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	UpdateDayNightCycle();
}


void AOpenGameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AOpenGameState, CurrentTime);
}

void AOpenGameState::UpdateDayNightCycle()
{
	if (GetNetMode() == NM_Client)
	{
		UKismetSystemLibrary::PrintString(GetWorld(), FString::Printf(
			                                  TEXT("当前时间 Days=%d Hours=%d"),
			                                  CurrentTime.Days, CurrentTime.Hours), true, true, FLinearColor(0, .66, 1),
		                                  1);
		return;
	}

	CurrentTime.Hours++;
	if (CurrentTime.Hours >= DayLength)
	{
		CurrentTime.Hours -= DayLength;
		CurrentTime.Days++;
	}

	Server_UpdateDayNightCycle(CurrentTime);
}

void AOpenGameState::Server_UpdateDayNightCycle_Implementation(const FTimeData& NewTime)
{
	// Multicast_UpdateDayNightCycle(NewTime);
	CurrentTime = NewTime;
}

// void AOpenGameState::Multicast_UpdateDayNightCycle_Implementation(const FTimeData& NewTime)
// {
// 	CurrentTime = NewTime;
// 	UKismetSystemLibrary::PrintString(GetWorld(), FString::Printf(
// 		                                  TEXT("Multicast_UpdateDayNightCycle CurrentTime Days=%d Hours=%d"),
// 		                                  CurrentTime.Days, CurrentTime.Hours));
// 	OnUpdateCurrentTime.Broadcast(CurrentTime);
// }


void AOpenGameState::TryToAddControllerTagWithHandle(const FGameplayTag& WeatherControllerTag,
                                                     const FEventWithWeatherHandle& Handle)
{
	WeatherControllerTagWithHandle.FindOrAdd(WeatherControllerTag) = Handle;
	// UKismetSystemLibrary::PrintString(GetWorld(), FString::Printf(
	// 	                                  TEXT(
	// 		                                  "TryToAddControllerTagWithHandle Controller Tag=%s EventTag=%s WeatherType=%s"),
	// 	                                  *WeatherControllerTag.ToString(), *Handle.Event.EventTag.ToString(),
	// 	                                  *UEnum::GetDisplayValueAsText(Handle.Weather.WeatherType).
	// 	                                  ToString()));
}


void AOpenGameState::Server_UpdateGlobalWeatherEventTag_Implementation(const FGameplayTag& NewGlobalWeatherEventTag)
{
	GlobalWeatherEventTag = NewGlobalWeatherEventTag;
	OnUpdateGlobalWeatherEvent.Broadcast(NewGlobalWeatherEventTag);
}
