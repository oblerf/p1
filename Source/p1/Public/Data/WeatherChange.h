// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"
#include "Engine/DataAsset.h"
#include "WeatherChange.generated.h"

USTRUCT(Blueprintable, BlueprintType)
struct FTimeData
{
	GENERATED_BODY()
	UPROPERTY(BlueprintReadOnly)
	int32 Hours = 0;
	UPROPERTY(BlueprintReadOnly)
	int32 Days = 1;

	FTimeData Add(const FTimeData& AnotherTime, const int32 DayLength) const
	{
		FTimeData NewTime = FTimeData();
		NewTime.Hours = Hours + AnotherTime.Hours;
		NewTime.Days = Days + (AnotherTime.Days + NewTime.Hours / DayLength);
		NewTime.Hours %= DayLength;
		return NewTime;
	}

	bool operator==(const FTimeData& AnotherTime) const
	{
		return Hours == AnotherTime.Hours && Days == AnotherTime.Days;
	}

	bool operator>(const FTimeData& AnotherTime) const
	{
		return Days > AnotherTime.Days || (Days == AnotherTime.Days && Hours > AnotherTime.Hours);
	}

	bool operator<(const FTimeData& AnotherTime) const
	{
		return Days < AnotherTime.Days || (Days == AnotherTime.Days && Hours < AnotherTime.Hours);
	}

	bool operator>=(const FTimeData& AnotherTime) const
	{
		return (*this) > AnotherTime || (*this) == AnotherTime;
	}

	bool operator<=(const FTimeData& AnotherTime) const
	{
		return (*this) < AnotherTime || (*this) == AnotherTime;
	}
};

UENUM(BlueprintType)
enum EWeatherType:uint8
{
	None,
	Sunny,
	Rainy,
	Snowy,
	Windy,
	BloodMoon,
	SolarEclipse,
};

USTRUCT(BlueprintType)
struct FWeatherData
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TEnumAsByte<EWeatherType> WeatherType = EWeatherType::None;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int32 Duration = 1;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int32 Prob = 0;

	UPROPERTY(BlueprintReadOnly)
	FTimeData EndTime = FTimeData();
};

USTRUCT(BlueprintType)
struct FWeatherEvent
{
	GENERATED_BODY()
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FGameplayTag EventTag = FGameplayTag();
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float Duration = 1.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FGameplayTag NextEventTag = FGameplayTag();
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TArray<FWeatherData> WeatherDataInfos;
	UPROPERTY(BlueprintReadOnly)
	FTimeData EndTime = FTimeData();
};

USTRUCT(BlueprintType)
struct FEventWithWeatherHandle
{
	GENERATED_BODY()
	UPROPERTY(BlueprintReadOnly)
	FWeatherEvent Event;
	UPROPERTY(BlueprintReadOnly)
	FWeatherData Weather;
};

/**
 * 
 */
UCLASS()
class P1_API UWeatherChange : public UDataAsset
{
	GENERATED_BODY()

public:
	UFUNCTION()
	bool RandWeatherFromEvent(const FGameplayTag& Event, const TEnumAsByte<EWeatherType>& Weather,
	                          FWeatherData& OutWeatherData, FWeatherEvent& OutEventData);

	UPROPERTY(EditDefaultsOnly)
	FGameplayTag WeatherDefaultEventTag;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TArray<FWeatherEvent> WeatherEvents;
};
