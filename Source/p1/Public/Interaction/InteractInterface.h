// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "p1/p1.h"
#include "UObject/Interface.h"
#include "InteractInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI, Blueprintable, BlueprintType)
class UInteractInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class P1_API IInteractInterface
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintNativeEvent)
	void UpdateOverlapEvent(const FGameplayTag& InteractTag);
	UFUNCTION(BlueprintNativeEvent)
	void OnBeginOverlapPlayerPawn();
	UFUNCTION(BlueprintNativeEvent)
	void OnEndOverlapPlayerPawn();

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
};
