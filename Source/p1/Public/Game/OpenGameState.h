// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"
#include "Data/WeatherChange.h"
#include "GameFramework/GameState.h"
#include "OpenGameState.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnUpdateCurrentTime, const FTimeData&, NewTime);

DECLARE_MULTICAST_DELEGATE_OneParam(FOnUpdateGlobalWeatherEvent, const FGameplayTag& /*EventTag*/);

/**
 * 
 */
UCLASS()
class P1_API AOpenGameState : public AGameState
{
	GENERATED_BODY()

public:
	AOpenGameState();
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	/*
	 * DayNightCycle
	 */
	UPROPERTY(Replicated, BlueprintReadOnly, Category="DayNightCycle")
	FTimeData CurrentTime;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="DayNightCycle")
	int32 DayLength = 24;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DayNightCycle")
	float RealSecondPerHour = 1.f;
	UPROPERTY(BlueprintReadOnly, Category="DayNightCycle")
	FOnUpdateCurrentTime OnUpdateCurrentTime;

	void UpdateDayNightCycle();

	/*
	* Weather Data
	*/
	UPROPERTY(EditDefaultsOnly, Category="Weather Data")
	TObjectPtr<UWeatherChange> WeatherChangeData;
	UPROPERTY(BlueprintReadOnly, Category="Weather Data")
	TMap<FGameplayTag, FEventWithWeatherHandle> WeatherControllerTagWithHandle;
	UPROPERTY(BlueprintReadOnly, Category="Weather Data")
	FGameplayTag GlobalWeatherEventTag;

	FOnUpdateGlobalWeatherEvent OnUpdateGlobalWeatherEvent;

	UFUNCTION(Server, Reliable)
	void Server_UpdateDayNightCycle(const FTimeData& NewTime);
	// UFUNCTION(NetMulticast, Reliable)
	// void Multicast_UpdateDayNightCycle(const FTimeData& NewTime);
	UFUNCTION()
	void TryToAddControllerTagWithHandle(const FGameplayTag& WeatherControllerTag,
	                                     const FEventWithWeatherHandle& Handle);

	UFUNCTION(BlueprintCallable, Server, Reliable)
	void Server_UpdateGlobalWeatherEventTag(const FGameplayTag& NewGlobalWeatherEventTag);
};
