// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"
#include "GameFramework/PlayerController.h"
#include "OpenPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class P1_API AOpenPlayerController : public APlayerController
{
	GENERATED_BODY()

public:

	UFUNCTION(Server, Reliable, BlueprintCallable)
	void UpdateOverlapEvent(AActor* Actor,const FGameplayTag& EventTag);
	UFUNCTION(Server, Reliable, BlueprintCallable)
	void UpdateGlobalEvent(const FGameplayTag& EventTag);

	UFUNCTION(Server, Reliable, BlueprintCallable)
	void PostPawnOnBeginOverlapActor(AActor* Actor);
	UFUNCTION(BlueprintCallable)
	void PostPawnOnEndOverlapActor(AActor* Actor);
};
