// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Data/WeatherChange.h"
#include "GameFramework/Actor.h"
#include "Interaction/InteractInterface.h"
#include "WeatherController.generated.h"


class UBoxComponent;


UCLASS()
class P1_API AWeatherController : public AActor, public IInteractInterface
{
	GENERATED_BODY()

public:
	AWeatherController();
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	//Trigger
	virtual void UpdateOverlapEvent_Implementation(const FGameplayTag& EventTag) override;
	virtual void OnBeginOverlapPlayerPawn_Implementation() override;
	virtual void OnEndOverlapPlayerPawn_Implementation() override;

	UPROPERTY(BlueprintReadOnly, ReplicatedUsing=OnRep_CurrentWeather)
	FWeatherData CurrentWeather;
	UPROPERTY(BlueprintReadOnly)
	FWeatherEvent CurrentEvent; //Only Update in server


	UPROPERTY(EditInstanceOnly, Category="Weather")
	FGameplayTag WCTag;

	UFUNCTION(Server, Reliable)
	void Server_UpdateWeather(const FGameplayTag& EventTag);
	UFUNCTION(NetMulticast, Reliable)
	void Multicast_ReceiveWeatherUpdate(const FWeatherData& Weather);
	UFUNCTION()
	void OnRep_CurrentWeather();

	UFUNCTION()
	void UpdateWeatherActive();
	UFUNCTION()
	bool CheckIfFirstPawnOverlap() const;

protected:
	UPROPERTY(BlueprintReadOnly)
	TMap<TEnumAsByte<EWeatherType>, TObjectPtr<UParticleSystemComponent>> WeatherParticleSystems;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	TObjectPtr<UBoxComponent> Box;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	TObjectPtr<UParticleSystemComponent> SunnySystem;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	TObjectPtr<UParticleSystemComponent> RainySystem;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	TObjectPtr<UParticleSystemComponent> SnowySystem;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	TObjectPtr<UParticleSystemComponent> WindySystem;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	TObjectPtr<UParticleSystemComponent> BloodMoonSystem;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	TObjectPtr<UParticleSystemComponent> SolarEclipseSystem;

	UFUNCTION()
	void GlobalWeatherEventUpdate (const FGameplayTag& EventTag);
};
